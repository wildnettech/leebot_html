$('.topupFundsPopup-Close').click(function(){
    $('.topupFundsPopup').hide();
});
$('a.btn-1.topstripbtn').click(function(){
    $('.topupFundsPopup').show();
});
/*---------- Number Timer  start  -----*/

/**
* jQuery scroroller Plugin 1.0
*
* http://www.tinywall.net/
* 
* Developers: Arun David, Boobalan
* Copyright (c) 2014 
*/
(function($){
    $(window).on("load",function(){
        $(document).scrollzipInit();
        $(document).rollerInit();
    });
    $(window).on("load scroll resize", function(){
        $('.numscroller').scrollzip({
            showFunction    :   function() {
                                    numberRoller($(this).attr('data-slno'));
                                },
            wholeVisible    :     false,
        });
    });
    $.fn.scrollzipInit=function(){
        $('body').prepend("<div style='position:fixed;top:0px;left:0px;width:0;height:0;' id='scrollzipPoint'></div>" );
    };
    $.fn.rollerInit=function(){
        var i=0;
        $('.numscroller').each(function() {
            i++;
           $(this).attr('data-slno',i); 
           $(this).addClass("roller-title-number-"+i);
        });        
    };
    $.fn.scrollzip = function(options){
        var settings = $.extend({
            showFunction    : null,
            hideFunction    : null,
            showShift       : 0,
            wholeVisible    : false,
            hideShift       : 0,
        }, options);
        return this.each(function(i,obj){
            $(this).addClass('scrollzip');
            if ( $.isFunction( settings.showFunction ) ){
                if(
                    !$(this).hasClass('isShown')&&
                    ($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.showShift)>($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))&&
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))<($(this).outerHeight()+$(this).offset().top-settings.showShift)
                ){
                    $(this).addClass('isShown');
                    settings.showFunction.call( this );
                }
            }
            if ( $.isFunction( settings.hideFunction ) ){
                if(
                    $(this).hasClass('isShown')&&
                    (($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.hideShift)<($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))||
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))>($(this).outerHeight()+$(this).offset().top-settings.hideShift))
                ){
                    $(this).removeClass('isShown');
                    settings.hideFunction.call( this );
                }
            }
            return this;
        });
    };
    function numberRoller(slno){
            var min=$('.roller-title-number-'+slno).attr('data-min');
            var max=$('.roller-title-number-'+slno).attr('data-max');
            var timediff=$('.roller-title-number-'+slno).attr('data-delay');
            var increment=$('.roller-title-number-'+slno).attr('data-increment');
            var numdiff=max-min;
            var timeout=(timediff*1000)/numdiff;
            //if(numinc<10){
                //increment=Math.floor((timediff*1000)/10);
            //}//alert(increment);
            numberRoll(slno,min,max,increment,timeout);
            
    }
    function numberRoll(slno,min,max,increment,timeout){//alert(slno+"="+min+"="+max+"="+increment+"="+timeout);
        if(min<=max){
            $('.roller-title-number-'+slno).html(min);
            min=parseInt(min)+parseInt(increment);
            setTimeout(function(){numberRoll(eval(slno),eval(min),eval(max),eval(increment),eval(timeout))},timeout);
        }else{
            $('.roller-title-number-'+slno).html(max);
        }
    }
})(jQuery);
/*---------- Number Timer  end  -----*/


/*---------- Circle Report start  -----*/
$( document ).ready(function() { 
             
if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) 
{
   console.log("Browser is Safari");          
   $("#test-circle1").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 5,
            backgroundBorderWidth: 1,
            percent: 90,           
            textSize: 20,           
            multiPercentage: 2,
            percentages: [10, 20, 30],
            animateInView: false
        });

        $("#test-circle2").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 5,
            backgroundBorderWidth: 1,
            percent: 60,            
            textSize: 28,            
            multiPercentage: 1,
            percentages: [10, 20, 30],
            animateInView: false

        });

        $("#test-circle3").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 5,
            backgroundBorderWidth: 1,
            percent: 70,           
            textSize: 28,           
            multiPercentage: 1,
            percentages: [10, 20, 30],
            animateInView: false

        });

        $("#test-circle4").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 3,
            backgroundBorderWidth: 1,
            percent: 80,           
            textSize: 28,           
            multiPercentage: 1,
            percentages: [10, 20, 30],
            animateInView: false

        });
}
else{
	$("#test-circle1").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 5,
            backgroundBorderWidth: 1,
            percent: 90,           
            textSize: 20,           
            multiPercentage: 2,
            percentages: [10, 20, 30],
            animateInView: true
        });

        $("#test-circle2").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 5,
            backgroundBorderWidth: 1,
            percent: 60,            
            textSize: 28,            
            multiPercentage: 1,
            percentages: [10, 20, 30],
            animateInView: true

        });

        $("#test-circle3").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 5,
            backgroundBorderWidth: 1,
            percent: 70,           
            textSize: 28,           
            multiPercentage: 1,
            percentages: [10, 20, 30],
            animateInView: true

        });

        $("#test-circle4").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 3,
            backgroundBorderWidth: 1,
            percent: 80,           
            textSize: 28,           
            multiPercentage: 1,
            percentages: [10, 20, 30],
            animateInView: true

        });
}

       /* if(true){
        	var ua = navigator.userAgent.toLowerCase(); 
			if (ua.indexOf('safari') != -1) { 
			  if (ua.indexOf('chrome') > -1) {
			    alert("1") // Chrome
			  } else {
			    alert("2") // Safari





			  }
			}
		}
*/
        
       
        $('.report-chart circle:nth-child(3)').attr('r', '40');
       // $('.circliful').append("<text x='0' y='15' fill='red' transform='rotate(30 20,40)'>I love SVG</text>");
       
    });
/*---------- Circle Report end  -----*/



/*--------- on-off-switch start -------------*/

if (!DG)var DG = {};

DG.OnOffSwitchAuto = function (config) {

    var properties = DG.OnOffSwitchProperties;

    $( document ).ready(function() {
        if(config.cls){
            var els = $(config.cls);
            var index = 0;
            for(var i=0,len=els.length;i<len;i++){
                var elementConfig = jQuery.extend({}, config);
                var el = $(els[i]);
                if(!els[i].id){
                    els[i].id = "dg-switch-" + index;
                    index++;
                }
                elementConfig.el = "#" + els[i].id

                for(var j=0;j<properties.length;j++){
                    var attr = "data-"+ properties[j];
                    var val = el.attr(attr);
                    if(val){
                        elementConfig[properties[j]] = val;
                    }
                }

                new DG.OnOffSwitch(
                    elementConfig
                );
            }
        }
    });


};

/*--------- on-off-switch end -------------*/



$(document).ready(function() {
    var $win = $(window),
        $win_height = $(window).height(),
        windowPercentage = ($(window).height() * 1.4) // - multiple of viewport height - The higher this number the sooner triggered.
    $win.on('scroll', scrollReveal);
    function scrollReveal() {
        var scrolled = $win.scrollTop();
        $(".graph-bar").each(function() {
            var $this = $(this),
                offsetTop = $this.offset().top;
            if (scrolled + windowPercentage > offsetTop || $win_height > offsetTop) {
                var mql = window.matchMedia("screen and (min-width: 100px)") // Placeholder media query
                if (mql.matches) { // if media query matches
                    var height1 = $(this).find("p").text(); // Getting the bar % from the text in the P
                    var height2 = height1.substr(0, 2); // using only the first 2 chars
                    var height3 = height2 * 2.07; // converting % to px - 206px is the height of the graph box - i.e. a 100% bar
                } else {
                    var height1 = $(this).find("p").text(); // Getting the bar % from the text in the P
                    var height2 = height1.substr(0, 2); // using only the first 2 chars
                    var height3 = height2 * 1.15; // converting % to px - 206px is the height of the graph box - i.e. a 100% bar
                }
                $(this).css("height", height3);
                $(this).animate({
                    'bottom': 0
                }, 1000, function() {
                    $(this).find("p").fadeIn(800);
                });
            }
        });
    }
    scrollReveal();
});
$(document).click(function(event) {
    var aa = event.target.id;
    if( aa !='typing'){
       $(".choosen-dropdown").removeClass('showC'); 
    }
    });
$(document).ready(function(event) {
    $(".choosen-dropdown").mCustomScrollbar({
        theme:"dark-3"
    });
    $('.choosen-input').click(function(event) {
      $(".choosen-dropdown").addClass('showC');
    });
    $('.choosen-sub').click(function(event) {       
        $(".choosen-dropdown").removeClass('showC');
        $('.choosedItem ul').append('<li>'+$(this).text()+'<span class=choosedItemClose><img src="images/closeBtn.png"></span></li>');
    });
    $('body').on('click','.choosedItemClose',function(event) {
      $(this).parent().remove();
    });





});

var months = new Array(12);
months[0] = "January";
months[1] = "February";
months[2] = "March";
months[3] = "April";
months[4] = "May";
months[5] = "June";
months[6] = "July";
months[7] = "August";
months[8] = "September";
months[9] = "October";
months[10] = "November";
months[11] = "December";
var current_date = new Date();
month_value = current_date.getMonth();
$('.monthName').text(months[month_value]);
var pmdSliderRangeTooltip = document.getElementById('pmd-slider-range-tooltip');
var startR = $(pmdSliderRangeTooltip).attr('data-start');
var newDate = new Date();  


function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

noUiSlider.create(pmdSliderRangeTooltip, {
    start: [newDate.getDate(), daysInMonth(newDate.getMonth(),newDate.getYear())],
    connect: true,
    tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
    range: {
        'min': newDate.getDate(),
        'max': daysInMonth(newDate.getMonth(),newDate.getYear())
    },
    step: 1,
   
    
});
